// SPDX-License-Identifier: LGPL-3.0-or-later

plugins {
    kotlin("multiplatform") version Versions.KOTLIN apply false
    kotlin("plugin.serialization") version Versions.KOTLIN apply false
}

allprojects {
    group = Publish.GROUP
    version = Versions.CORE
    repositories {
        mavenCentral()
        jcenter()
        mavenLocal()
    }
    configurations.create("compileClasspath")
}
