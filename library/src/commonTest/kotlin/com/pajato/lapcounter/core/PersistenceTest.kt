package com.pajato.lapcounter.core

import kotlin.test.Test
import kotlin.test.assertFailsWith

class PersistenceTest {

    @Test fun `when creating an empty session persistence directory verify illegal argument exception is thrown`() {
        assertFailsWith<IllegalArgumentException> { LapCounter("") }
    }

    @Test fun `when creating an empty session persistence file name verify illegal argument exception is thrown`() {
        assertFailsWith<IllegalArgumentException> { LapCounter("someDir", "") }
    }

    @Test fun `when creating an empty session persistence file verify illegal state exception is thrown`() {
        val dir = "./library/src/commonTest/resources/session-repo-empty"
        assertFailsWith<IllegalStateException> { LapCounter(dir) }
    }
}