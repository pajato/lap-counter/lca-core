package com.pajato.lapcounter.core

actual fun Double.convertToString(): String = "%.2f".format(this)