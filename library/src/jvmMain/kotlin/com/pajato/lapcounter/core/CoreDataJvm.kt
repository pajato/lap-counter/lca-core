package com.pajato.lapcounter.core

actual fun getTimeStamp(): TimeStamp = System.currentTimeMillis()
