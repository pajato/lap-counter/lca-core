package com.pajato.lapcounter.core

typealias TimeStamp = Long

expect fun getTimeStamp(): TimeStamp

const val ILLEGAL_DIRECTORY_NAME_MESSAGE = "The persistence directory must not be  blank!"
const val ILLEGAL_FILE_NAME_MESSAGE = "The persistence file name must not be blank!"
const val PERSISTENCE_VERSION = 1

const val CRUISE_FILE_NAME = "cruise-selection"
const val SHIP_FILE_NAME = "ship-selection"

const val baseImageURL = "https://www.vikingrivercruises.com/images/"
