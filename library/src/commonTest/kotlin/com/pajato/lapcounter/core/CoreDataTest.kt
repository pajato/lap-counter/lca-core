package com.pajato.lapcounter.core

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class CoreDataTest {
    @Test fun `When using a blank receiver on toSession verify an IllegalStateException is thrown`() {
        assertFailsWith<IllegalStateException> { "".toSession(1) }
    }

    @Test fun `When using a non blank but invalid receiver on toSession verify an IllegalStateException is thrown`() {
        assertFailsWith<IllegalStateException> { "1:2".toSession(0) }
    }

    @Test fun `When using a blank receiver on toSession verify the exception message is correct`() {
        try {
            "".toSession(1)
        } catch (exc: IllegalStateException) {
            assertEquals("Invalid session string: .", exc.message)
        }
    }

    @Test fun `When using an unsupported version verify the exception message is correct`() {
        try {
            "1:2:Rhine Getaway:Sigrun:0::".toSession(-1)
        } catch (exc: IllegalStateException) {
            assertEquals("Unsupported version: -1.", exc.message)
        }
    }

    @Test fun `When using a valid receiver on toSession verify the correct return value`() {
        val uut = "1:2:Rhine Getaway:Sigrun:0::".toSession(1)
        assertEquals("1", uut.start)
        assertEquals("2", uut.end)
        assertEquals("Rhine Getaway", uut.cruiseName)
        assertEquals("Sigrun", uut.shipName)
        assertEquals("", uut.startLocation)
        assertEquals("", uut.endLocation)
    }
}
