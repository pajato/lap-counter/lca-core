# lca-core

A library providing common core components for the Android and iOS lap counter apps.

Publish this library to a local maven repository before using it to build the Android or iOS apps.
