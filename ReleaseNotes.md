Note: all versions up to 0.1.1 were not persisted.

Version 0.1.9

    Upgrade to Kotlin release 1.3.71
    
Version 0.1.8

    Upgrade to Kotlin release 1.3.70

Version 0.1.7

    Remove "viking" from all package names.

    Use a double ended arrow from the extended UTF-8 symbol character set.

Version 0.1.6

    Make the file name optional when instantiating cruise and ship selection classes as well as the lap counter class.

    Expose Selector#isSelectable() and getItemIndexFromName() methods.

    Allow Selector#selectedItem to be set to the default (empty) item for testing.

    Move filename constants from SelectorTest.kt to CoreData.kt

    Ensure that the object repository does not contain blank cruise or ship names.

    When encoutering an invalid name during initialization default the selected item to "".

Version 0.1.5

    Meke isSelector public so that category items can be treadted differently from selectable items.

Version 0.1.4

    Take a default argument on Selector#getItemIndexFromName() and make it public access.

Version 0.1.6

    Expose Selector#isSelectable() and getItemIndexFromName() methods.

    Allow Selector#selectedItem to be set to the default (empty) item for testing.

    Move filename constants from SelectorTest.kt to CoreData.kt

    Ensure that the object repository does not contain blank cruise or ship names.

Version 0.1.5

    Meke isSelector public so that category items can be treadted differently from selectable items.

Version 0.1.4

    Take a default argument on Selector#getItemIndexFromName() and make it public access.

Version 0.1.3

    Consolidate Cruise and Ship selection code

Version 0.1.2

    Base code for subsequent releases
