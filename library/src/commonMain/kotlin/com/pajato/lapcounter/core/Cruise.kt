package com.pajato.lapcounter.core

internal enum class CruiseItems(override val item: SelectorItem, override val list: List<SelectorItem>) : SelectorType {
    DANUBE(
        CategoryItem("Danube", "CC_Budapest_Parliament_Pink_RM_478x345_tcm21-79042.jpg"),
        listOf<SelectorItem>(
            Cruise.GRAND_EUROPEAN_TOUR.item,
            Cruise.ROMANTIC_DANUBE.item,
            Cruise.DANUBE_WALTZ.item,
            Cruise.PASSAGE_TO_EASTERN_EUROPE.item,
            Cruise.EUROPEAN_SOJOURN.item,
            Cruise.OBERAMMERGAU_THE_PASSION_PLAY.item,
            Cruise.OBERAMMERGAU_WITH_SALZBURG.item,
            Cruise.OBERAMMERGAU_AND_ROMANTIC_DANUBE.item,
            Cruise.OBERAMMERGAU_SALZBURG_AND_CRUISE.item,
            Cruise.OBERAMMERGAU_SALZBURG_THE_DANUBE.item,
            Cruise.OBERAMMERGAU_AND_DANUBE_WALTZ.item,
            Cruise.GRAND_EUROPEAN_AND_VIKING_FJORDS.item
        )
    ),
    RHINE(
        CategoryItem("Rhine", "Rhine_Katz_Castle_Aerial_Alamy_RM_478x345_v2_tcm21-58689.jpg"),
        listOf<SelectorItem>(
            Cruise.GRAND_EUROPEAN_TOUR.item,
            Cruise.RHINE_GETAWAY.item,
            Cruise.PARIS_TO_THE_SWISS_ALPS.item,
            Cruise.CITIES_OF_LIGHT.item,
            Cruise.TULIPS_AND_WINDMILLS.item,
            Cruise.HOLLAND_AND_BELGIUM.item,
            Cruise.EUROPEAN_SOJOURN.item,
            Cruise.OBERAMMERGAU_THE_ALPS_AND_THE_RHINE.item,
            Cruise.OBERAMMERGAU_INNSBRUCK_AND_THE_RHINE.item,
            Cruise.RHINE_AND_VIKING_SHORES_AND_FJORDS.item
        )
    )
    //FRANCE,
    //DOURO("Douro"),
    //ELBE("Elbe"),
    //HOLIDAY,
    //RUSSIA,
    //UKRAINE,
    //ASIA,
    //EGYPT,
    //RIVER_PLUS_OCEAN
}

internal enum class Cruise(val item: CruiseItem) {
    GRAND_EUROPEAN_TOUR(
        CruiseItem(
            "Grand European Tour",
            "CC_Wachau_Aggstein_Castle_Danube_Sunset_Alamy_RM_478x345_tcm21-109376.jpg",
            "Amsterdam", "Budapest"
        )
    ),
    ROMANTIC_DANUBE(
        CruiseItem(
            "Romantic Danube",
            "CC_BUD_Parliament_River_Bridge_478x345_tcm21-9369.jpg",
            "Budapest", "Nuremberg"
        )
    ),
    DANUBE_WALTZ(
        CruiseItem(
            "Danube Waltz",
            "Krems_Gottweig_Abbey_478x345_tcm21-106344.jpg",
            "Passau",
            "Budapest"
        )
    ),
    PASSAGE_TO_EASTERN_EUROPE(
        CruiseItem(
            "Passage to Eastern Europe",
            "CC_BelgradeStSavaChurchWFountains_SKY_478x345_tcm21-9321.jpg",
            "Bucharest",
            "Budapest"
        )
    ),
    EUROPEAN_SOJOURN(
        CruiseItem(
            "European Sojourn",
            "CC_BambergGermanyBridgeRZU_SKY_478x345_tcm21-9327.jpg",
            "Amsterdam",
            "Bucharest"
        )
    ),
    OBERAMMERGAU_THE_PASSION_PLAY(
        CruiseItem(
            "Oberammergau, The Passion Play",
            "Oberammergau_Passion_Play_Jesus_Priests_AGE_RM_478x345_tcm21-112497.jpg",
            "Munich",
            "Budapest"
        )
    ),
    OBERAMMERGAU_WITH_SALZBURG(
        CruiseItem(
            "Oberammergau with Salzburg",
            "Passion_Play_Last_Supper_478x345_tcm21-124428.jpg",
            "Munich",
            "Budapest"
        )
    ),
    OBERAMMERGAU_AND_ROMANTIC_DANUBE(
        CruiseItem(
            "Oberammergau & Romantic Danube",
            "Oberammergau_Passion_Play_Religious_Man_Alamy_RM_478x345_tcm21-129246.jpg",
            "Oberammergau",
            "Budapest"
        )
    ),
    OBERAMMERGAU_SALZBURG_AND_CRUISE(
        CruiseItem(
            "Oberammergau, Salzburg & Cruise",
            "Oberammergau_Passion_Play_Scene_Alamy_RM_478x345_tcm21-128156.jpg",
            "Oberammergau",
            "Budapest"
        )
    ),
    OBERAMMERGAU_SALZBURG_THE_DANUBE(
        CruiseItem(
            "Oberammergau, Salzburg & the Danube",
            "Oberammergau_Passion_Play_Jesus_Alamy_RM_478x345_tcm21-129260.jpg",
            "Oberammergau",
            "Budapest"
        )
    ),
    OBERAMMERGAU_AND_DANUBE_WALTZ(
        CruiseItem(
            "Oberammergau & Danube Waltz",
            "Oberammergau_Passion_Play_Original_Sin_Alamy_RM_478x345_tcm21-128151.jpg",
            "Oberammergau",
            "Budapest"
        )
    ),
    GRAND_EUROPEAN_AND_VIKING_FJORDS(
        CruiseItem(
            "Grand European & Viking Fjords",
            "CC_Wachau_Aggstein_Castle_Danube_Sunset_Alamy_RM_478x345_tcm21-109376.jpg",
            "Bergen",
            "Budapest"
        )
    ),
    RHINE_GETAWAY(
        CruiseItem(
            "Rhine Getaway",
            "Rhine_MarksburgCastleAerialCC_478x345_tcm21-9363.jpg",
            "Amsterdam",
            "Basel"
        )
    ),
    PARIS_TO_THE_SWISS_ALPS(
        CruiseItem(
            "Paris to the Swiss Alps",
            "Zurich_City_Bridge_Dusk_478x345_tcm21-58330.jpg",
            "Paris",
            "Zürich"
        )
    ),
    CITIES_OF_LIGHT(
        CruiseItem(
            "Cities of Light",
            "CCPragueAtSundown_478x345_tcm21-9309.jpg",
            "Paris",
            "Prague"
        )
    ),
    TULIPS_AND_WINDMILLS(
        CruiseItem(
            "Tulips & Windmills",
            "CC_Holland_Windmill_Tulips_SKY_478x345-min_tcm21-9375.jpg",
            "Amsterdam",
            "Amsterdam"
        )
    ),
    HOLLAND_AND_BELGIUM(
        CruiseItem(
            " Holland & Belgium",
            "Amsterdam_Canal_Tulips_Bicycles_478x345_tcm21-141565.jpg",
            "Amsterdam",
            "Antwerp"
        )
    ),
    OBERAMMERGAU_THE_ALPS_AND_THE_RHINE(
        CruiseItem(
            "Oberammergau, the Alps & the Rhine",
            "Oberammergau_Passion_Play_Judas_Alamy_RM_478x345_tcm21-138463.jpg",
            "Amsterdam",
            "Oberammergau"
        )
    ),
    OBERAMMERGAU_INNSBRUCK_AND_THE_RHINE(
        CruiseItem(
            "Oberammergau, Innsbruck & the Rhine",
            "Passion_Play_Performance_Alamy_RM_478x345_tcm21-135675.jpg",
            "Amsterdam",
            "Oberammergau"
        )
    ),
    RHINE_AND_VIKING_SHORES_AND_FJORDS(
        CruiseItem(
            "Rhine & Viking Shores & Fjords",
            "Stavanger_Lysefjord_Cliffs_Alamy_RM_478x345_tcm21-97769.jpg",
            "Basel",
            "Bergen"
        )
    )
}
