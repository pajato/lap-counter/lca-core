package com.pajato.lapcounter.core

interface SelectorType {
    val item: SelectorItem
    val list: List<SelectorItem>
}

sealed class SelectorItem(val name: String, val imageName: String = "", val imageUrl: String = baseImageURL + imageName)

class CategoryItem(name: String, imageName: String = "") : SelectorItem(name, imageName)

class CruiseItem(name: String, imageName: String, val city1: String, val city2: String) : SelectorItem(name, imageName)

class ShipItem(name: String, val lapsPerMile: Int = 12) : SelectorItem(name)

class DefaultItem : SelectorItem("")

class CruiseSelector(persistenceDir: String, persistenceFileName: String = "cruise-selection") :
    Selector(persistenceDir, persistenceFileName) {
    override val list = getItems(CruiseItems.values())

    init {
        initializeSelectedItem()
    }
}

class ShipSelector(persistenceDir: String, persistenceFileName: String = "ship-selection") :
    Selector(persistenceDir, persistenceFileName) {
    override val list = getItems(LongshipCategory.values())

    init {
        initializeSelectedItem()
    }
}

abstract class Selector(persistenceDir: String, persistenceFileName: String) {
    internal open inner class SelectionPersister(
        persistenceDir: String,
        persistenceFileName: String,
        loader: (Int, String) -> Unit
    ) : Persister(persistenceDir, persistenceFileName, loader)

    abstract val list: List<SelectorItem>

    var selectedItem: SelectorItem = DefaultItem()
        set(value) {
            fun setAndPersist() {
                if (value.name.isNotEmpty()) persister.store(value.name)
                field = value
            }

            when (value) {
                is CruiseItem -> setAndPersist()
                is ShipItem -> setAndPersist()
                is DefaultItem -> field = value
                is CategoryItem -> return
            }
        }

    private var repo = mutableListOf<String>()
    private val persister = SelectionPersister(persistenceDir, persistenceFileName) { _, line ->
        if (line.isNotEmpty()) repo.add(line)
    }

    fun initializeSelectedItem() {
        if (repo.size > 0) {
            val lastItemIndex = repo.size - 1
            val itemIndex = getItemIndexFromName(repo[lastItemIndex])

            if (itemIndex == -1) return
            selectedItem = list[itemIndex]
            repo.clear()
            persister.prune { persister.store(selectedItem.name) }
        }
    }

    fun <E : Enum<E>> getItems(values: Array<E>): List<SelectorItem> {
        val result = mutableListOf<SelectorItem>()
        fun addItemsFrom(type: SelectorType) {
            result.add(type.item)
            result.addAll(type.list)
        }

        values.forEach { addItemsFrom(it as SelectorType) }
        return result
    }

    fun isSelectable(item: SelectorItem): Boolean = item is CruiseItem || item is ShipItem

    fun getDescription(item: SelectorItem) = if (item is CruiseItem) "${item.city1} \u21C4 ${item.city2}" else ""

    fun getItemIndexFromName(name: String = selectedItem.name): Int {
        for (index in list.indices) if (list[index].name == name) return index
        return -1
    }
}
