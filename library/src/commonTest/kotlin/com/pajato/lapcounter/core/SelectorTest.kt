package com.pajato.lapcounter.core

import com.pajato.io.createKotlinFile
import com.pajato.lapcounter.core.Cruise.*
import com.pajato.lapcounter.core.CruiseItems.DANUBE
import com.pajato.lapcounter.core.CruiseItems.RHINE
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

const val CRUISE_NAME = "Grand European Tour"
const val CRUISE_SELECTION_FILE_NAME = "test-cruise-selection"
const val SHIP_NAME = "Sygrun"
const val SHIP_SELECTION_FILE_NAME = "test-ship-selection"

class SelectorTester(persistenceDir: String, fileName: String) : Selector(persistenceDir, fileName) {

    enum class TestItems(override val item: SelectorItem, override val list: List<SelectorItem>) : SelectorType {
        MYSTIC(
            CategoryItem("Mystic", "CC_Budapest_Parliament_Pink_RM_478x345_tcm21-79042.jpg"),
            listOf<SelectorItem>(DANUBE_WALTZ.item)
        ),
        CHARLES(
            CategoryItem("Charles", "Rhine_Katz_Castle_Aerial_Alamy_RM_478x345_v2_tcm21-58689.jpg"),
            listOf<SelectorItem>(CITIES_OF_LIGHT.item)
        )
    }
    override val list = getItems(TestItems.values())
    init {
        initializeSelectedItem()
    }
}

class SelectorTest {

    private val testDir = "build"
    private val cruiseFile = createKotlinFile(testDir, CRUISE_FILE_NAME)
    private val shipFile = createKotlinFile(testDir, SHIP_FILE_NAME)

    @BeforeTest
    fun setup() {
        cruiseFile.apply {
            clear()
            appendText("$PERSISTENCE_VERSION\n")
        }
        shipFile.apply {
            clear()
            appendText("$PERSISTENCE_VERSION\n")
        }
    }

    @Test
    fun `When no item is persisted verify the selected item is empty`() {
        val uutCruise = CruiseSelector(testDir, CRUISE_SELECTION_FILE_NAME)
        val uutShip = ShipSelector(testDir, SHIP_SELECTION_FILE_NAME)
        assertEquals("", uutCruise.selectedItem.name)
        assertEquals("", uutShip.selectedItem.name)
    }

    @Test
    fun `When a category item is selected verify the selected item is unchanged`() {
        cruiseFile.appendText("Danube")
        val uutCruise = CruiseSelector(testDir, CRUISE_SELECTION_FILE_NAME)
        val uutShip = ShipSelector(testDir, SHIP_SELECTION_FILE_NAME)
        fun verifyItemIsEmpty(uut: Selector, item: SelectorItem) {
            val expected = uut.selectedItem
            uut.selectedItem = item
            assertEquals(expected, uut.selectedItem)
        }

        listOf("Danube", "Rhine").forEach { verifyItemIsEmpty(uutCruise, CategoryItem(it)) }
        listOf("A", "B", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "R", "S", "T", "U", "V").forEach {
            verifyItemIsEmpty(uutShip, CategoryItem(it))
        }
    }

    @Test
    fun `When a cruise and ship are selected verify the selected item`() {
        cruiseFile.apply {
            clear()
            appendText("${PERSISTENCE_VERSION}\n$CRUISE_NAME\n")
        }
        assertEquals(2, cruiseFile.readLines().size)
        val uutCruise = CruiseSelector(testDir)
        assertEquals(CRUISE_NAME, uutCruise.selectedItem.name)

        shipFile.apply {
            clear()
            appendText("${PERSISTENCE_VERSION}\n$SHIP_NAME\n")
        }
        assertEquals(2, cruiseFile.readLines().size)
        val uutShip = ShipSelector(testDir)
        assertEquals(SHIP_NAME, uutShip.selectedItem.name)
    }

    @Test
    fun `Verify the reverse index lookup with a valid cruise and ship names`() {
        val uutCruise = CruiseSelector(testDir, CRUISE_SELECTION_FILE_NAME)
        val uutShip = ShipSelector(testDir, SHIP_SELECTION_FILE_NAME)
        assertEquals(1, uutCruise.getItemIndexFromName(CRUISE_NAME))
        assertEquals(55, uutShip.getItemIndexFromName(SHIP_NAME))
        assertEquals(-1, uutCruise.getItemIndexFromName())
    }

    @Test
    fun `Verify the reverse index lookup with an invalid cruise name`() {
        val uutCruise = CruiseSelector(testDir, CRUISE_SELECTION_FILE_NAME)
        val uutShip = ShipSelector(testDir, SHIP_SELECTION_FILE_NAME)
        assertEquals(-1, uutCruise.getItemIndexFromName("No Such Cruise"))
        assertEquals(-1, uutShip.getItemIndexFromName("No Such Ship"))
    }

    @Test
    fun `When a valid cruise and ship item is used verify it reports as a cruise and a ship`() {
        val uutCruise = CruiseSelector(testDir, CRUISE_SELECTION_FILE_NAME)
        val uutShip = ShipSelector(testDir, SHIP_SELECTION_FILE_NAME)
        assertTrue(uutCruise.isSelectable(GRAND_EUROPEAN_TOUR.item))
        assertTrue(uutShip.isSelectable(Ship.SYGYN.item))
    }

    @Test
    fun `When an invalid cruise name is used verify it does not report as a cruise`() {
        val uutCruise = CruiseSelector(testDir, CRUISE_SELECTION_FILE_NAME)
        val uutShip = ShipSelector(testDir, SHIP_SELECTION_FILE_NAME)
        assertFalse(uutCruise.isSelectable(DANUBE.item))
        assertFalse(uutShip.isSelectable(LongshipCategory.A.item))
    }

    @Test
    fun `Verify accessing descriptions works correctly`() {
        val uutCruise = CruiseSelector(testDir, CRUISE_SELECTION_FILE_NAME)
        assertEquals("Amsterdam \u21C4 Basel", uutCruise.getDescription(RHINE_GETAWAY.item))
        assertEquals("", uutCruise.getDescription(DANUBE.item))
    }

    @Test
    fun `verify isSelectable`() {
        val uutCruise = CruiseSelector(testDir, CRUISE_SELECTION_FILE_NAME)
        assertTrue(uutCruise.isSelectable(GRAND_EUROPEAN_TOUR.item))
        assertFalse(uutCruise.isSelectable(RHINE.item))
        assertFalse(uutCruise.isSelectable(DefaultItem()))

        val uutShip = ShipSelector(testDir, SHIP_SELECTION_FILE_NAME)
        assertTrue(uutShip.isSelectable(Ship.SYGYN.item))
        assertFalse(uutShip.isSelectable(LongshipCategory.S.item))
        assertFalse(uutShip.isSelectable(DefaultItem()))
    }

    @Test fun `cover hard to get code`() {
        println("Executing coverage for fast exit ...")
        SelectorTester(testDir, "sessions")
    }

    @Test
    fun `When presenting the cruise items ensure the order and content is correct`() {
        fun verifyCruiseCategory(uutCruise: SelectorItem, type: CruiseItems) {
            assertEquals(type.item.name, uutCruise.name)
            assertEquals(type.item.imageName, uutCruise.imageName)
            val expectedUrl = baseImageURL + type.item.imageName
            assertEquals(expectedUrl, uutCruise.imageUrl)
        }

        fun verifyCruise(uutCruise: SelectorItem, type: Cruise) {
            assertTrue(uutCruise is CruiseItem)
            assertEquals(type.item.name, uutCruise.name)
            assertEquals(type.item.imageName, uutCruise.imageName)
            assertEquals(type.item.city1, uutCruise.city1)
            assertEquals(type.item.city2, uutCruise.city2)
            val expectedUrl = baseImageURL + type.item.imageName
            assertEquals(expectedUrl, uutCruise.imageUrl)
        }

        val uutCruise = CruiseSelector(testDir, CRUISE_SELECTION_FILE_NAME).list
        val expectedCruiseItems = 24

        assertEquals(expectedCruiseItems, uutCruise.size)
        verifyCruiseCategory(uutCruise[0], DANUBE)
        verifyCruise(uutCruise[1], GRAND_EUROPEAN_TOUR)
        verifyCruise(uutCruise[2], ROMANTIC_DANUBE)
        verifyCruise(uutCruise[3], DANUBE_WALTZ)
        verifyCruise(uutCruise[4], PASSAGE_TO_EASTERN_EUROPE)
        verifyCruise(uutCruise[5], EUROPEAN_SOJOURN)
        verifyCruise(uutCruise[6], OBERAMMERGAU_THE_PASSION_PLAY)
        verifyCruise(uutCruise[7], OBERAMMERGAU_WITH_SALZBURG)
        verifyCruise(uutCruise[8], OBERAMMERGAU_AND_ROMANTIC_DANUBE)
        verifyCruise(uutCruise[11], OBERAMMERGAU_AND_DANUBE_WALTZ)
        verifyCruise(uutCruise[12], GRAND_EUROPEAN_AND_VIKING_FJORDS)
        verifyCruiseCategory(uutCruise[13], RHINE)
        verifyCruise(uutCruise[14], GRAND_EUROPEAN_TOUR)
        verifyCruise(uutCruise[15], RHINE_GETAWAY)
        verifyCruise(uutCruise[16], PARIS_TO_THE_SWISS_ALPS)
        verifyCruise(uutCruise[17], CITIES_OF_LIGHT)
        verifyCruise(uutCruise[18], TULIPS_AND_WINDMILLS)
        verifyCruise(uutCruise[19], HOLLAND_AND_BELGIUM)
        verifyCruise(uutCruise[20], EUROPEAN_SOJOURN)
        verifyCruise(uutCruise[21], OBERAMMERGAU_THE_ALPS_AND_THE_RHINE)
        verifyCruise(uutCruise[22], OBERAMMERGAU_INNSBRUCK_AND_THE_RHINE)
        verifyCruise(uutCruise[23], RHINE_AND_VIKING_SHORES_AND_FJORDS)
    }

    @Test
    fun `When presenting the ship items ensure the order and content is correct`() {
        fun verifyShipCategory(uutShip: SelectorItem, type: LongshipCategory) {
            assertEquals(type.item.name, uutShip.name)
            assertEquals(type.item.imageName, uutShip.imageName)
            val expectedUrl = baseImageURL + type.item.imageName
            assertEquals(expectedUrl, uutShip.imageUrl)
        }

        fun verifyShip(uutShip: SelectorItem, type: Ship) {
            assertTrue(uutShip is ShipItem)
            assertEquals(type.item.name, uutShip.name)
            assertEquals(12, uutShip.lapsPerMile)
        }

        val uutShip = ShipSelector(testDir, SHIP_SELECTION_FILE_NAME).list
        val expectedShipItems = 72
        var i = 0

        assertEquals(expectedShipItems, uutShip.size)
        verifyShipCategory(uutShip[i++], LongshipCategory.A)
        verifyShip(uutShip[i++], Ship.AEGIR)
        verifyShip(uutShip[i++], Ship.ALRUNA)
        verifyShip(uutShip[i++], Ship.ALSVIN)
        verifyShip(uutShip[i++], Ship.ATLA)
        verifyShipCategory(uutShip[i++], LongshipCategory.B)
        verifyShip(uutShip[i++], Ship.BALDUR)
        verifyShip(uutShip[i++], Ship.BRAGI)
        verifyShip(uutShip[i++], Ship.BURI)
        verifyShipCategory(uutShip[i++], LongshipCategory.D)
        verifyShip(uutShip[i++], Ship.DELLING)
        verifyShipCategory(uutShip[i++], LongshipCategory.E)
        verifyShip(uutShip[i++], Ship.EGIL)
        verifyShip(uutShip[i++], Ship.EINAR)
        verifyShip(uutShip[i++], Ship.EIR)
        verifyShip(uutShip[i++], Ship.EMBLA)
        verifyShipCategory(uutShip[i++], LongshipCategory.F)
        verifyShip(uutShip[i++], Ship.FORSETI)
        verifyShipCategory(uutShip[i++], LongshipCategory.G)
        verifyShip(uutShip[i++], Ship.GEFJON)
        verifyShip(uutShip[i++], Ship.GERSEMI)
        verifyShip(uutShip[i++], Ship.GULLVEIG)
        verifyShipCategory(uutShip[i++], LongshipCategory.H)
        verifyShip(uutShip[i++], Ship.HEIMDAL)
        verifyShip(uutShip[i++], Ship.HERJA)
        verifyShip(uutShip[i++], Ship.HERMOD)
        verifyShip(uutShip[i++], Ship.HERVOR)
        verifyShip(uutShip[i++], Ship.HILD)
        verifyShip(uutShip[i++], Ship.HILN)
        verifyShipCategory(uutShip[i++], LongshipCategory.I)
        verifyShip(uutShip[i++], Ship.IDI)
        verifyShip(uutShip[i++], Ship.IDUN)
        verifyShip(uutShip[i++], Ship.INGVI)
        verifyShipCategory(uutShip[i++], LongshipCategory.J)
        verifyShip(uutShip[i++], Ship.JARL)
        verifyShipCategory(uutShip[i++], LongshipCategory.K)
        verifyShip(uutShip[i++], Ship.KADIN)
        verifyShip(uutShip[i++], Ship.KARA)
        verifyShip(uutShip[i++], Ship.KVASIR)
        verifyShipCategory(uutShip[i++], LongshipCategory.L)
        verifyShip(uutShip[i++], Ship.LIF)
        verifyShip(uutShip[i++], Ship.LOFN)
        verifyShipCategory(uutShip[i++], LongshipCategory.M)
        verifyShip(uutShip[i++], Ship.MAGNI)
        verifyShip(uutShip[i++], Ship.MANI)
        verifyShip(uutShip[i++], Ship.MIMIR)
        verifyShip(uutShip[i++], Ship.MODI)
        verifyShipCategory(uutShip[i++], LongshipCategory.N)
        verifyShip(uutShip[i++], Ship.NJORD)
        verifyShipCategory(uutShip[i++], LongshipCategory.O)
        verifyShip(uutShip[i++], Ship.ODIN)
        verifyShipCategory(uutShip[i++], LongshipCategory.R)
        verifyShip(uutShip[i++], Ship.RINDA)
        verifyShip(uutShip[i++], Ship.ROLF)
        verifyShipCategory(uutShip[i++], LongshipCategory.S)
        verifyShip(uutShip[i++], Ship.SYGRUN)
        verifyShip(uutShip[i++], Ship.SYGYN)
        verifyShip(uutShip[i++], Ship.SKADI)
        verifyShip(uutShip[i++], Ship.SKIRNIR)
        verifyShipCategory(uutShip[i++], LongshipCategory.T)
        verifyShip(uutShip[i++], Ship.TIALFI)
        verifyShip(uutShip[i++], Ship.TIR)
        verifyShip(uutShip[i++], Ship.TOR)
        verifyShipCategory(uutShip[i++], LongshipCategory.U)
        verifyShip(uutShip[i++], Ship.ULUR)
        verifyShipCategory(uutShip[i++], LongshipCategory.V)
        verifyShip(uutShip[i++], Ship.VALI)
        verifyShip(uutShip[i++], Ship.VAR)
        verifyShip(uutShip[i++], Ship.VE)
        verifyShip(uutShip[i++], Ship.VIDAR)
        verifyShip(uutShip[i++], Ship.VILHJALM)
        verifyShip(uutShip[i], Ship.VILI)
    }
}
