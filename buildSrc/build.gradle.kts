// SPDX-License-Identifier: LGPL-3.0-or-later

@file:Suppress("UnstableApiUsage")

plugins {
    `kotlin-dsl`
}

kotlinDslPluginOptions { experimentalWarning.set(false) }

repositories { jcenter() }
