package com.pajato.lapcounter.core

import com.pajato.lapcounter.core.Ship.AEGIR
import com.pajato.lapcounter.core.Ship.ALRUNA
import com.pajato.lapcounter.core.Ship.ALSVIN
import com.pajato.lapcounter.core.Ship.ATLA
import com.pajato.lapcounter.core.Ship.BALDUR
import com.pajato.lapcounter.core.Ship.BRAGI
import com.pajato.lapcounter.core.Ship.BURI
import com.pajato.lapcounter.core.Ship.DELLING
import com.pajato.lapcounter.core.Ship.EGIL
import com.pajato.lapcounter.core.Ship.EINAR
import com.pajato.lapcounter.core.Ship.EIR
import com.pajato.lapcounter.core.Ship.EMBLA
import com.pajato.lapcounter.core.Ship.FORSETI
import com.pajato.lapcounter.core.Ship.GEFJON
import com.pajato.lapcounter.core.Ship.GERSEMI
import com.pajato.lapcounter.core.Ship.GULLVEIG
import com.pajato.lapcounter.core.Ship.HEIMDAL
import com.pajato.lapcounter.core.Ship.HERJA
import com.pajato.lapcounter.core.Ship.HERMOD
import com.pajato.lapcounter.core.Ship.HERVOR
import com.pajato.lapcounter.core.Ship.HILD
import com.pajato.lapcounter.core.Ship.HILN
import com.pajato.lapcounter.core.Ship.IDI
import com.pajato.lapcounter.core.Ship.IDUN
import com.pajato.lapcounter.core.Ship.INGVI
import com.pajato.lapcounter.core.Ship.JARL
import com.pajato.lapcounter.core.Ship.KADIN
import com.pajato.lapcounter.core.Ship.KARA
import com.pajato.lapcounter.core.Ship.KVASIR
import com.pajato.lapcounter.core.Ship.LIF
import com.pajato.lapcounter.core.Ship.LOFN
import com.pajato.lapcounter.core.Ship.MAGNI
import com.pajato.lapcounter.core.Ship.MANI
import com.pajato.lapcounter.core.Ship.MIMIR
import com.pajato.lapcounter.core.Ship.MODI
import com.pajato.lapcounter.core.Ship.NJORD
import com.pajato.lapcounter.core.Ship.ODIN
import com.pajato.lapcounter.core.Ship.RINDA
import com.pajato.lapcounter.core.Ship.ROLF
import com.pajato.lapcounter.core.Ship.SYGRUN
import com.pajato.lapcounter.core.Ship.SYGYN
import com.pajato.lapcounter.core.Ship.SKADI
import com.pajato.lapcounter.core.Ship.SKIRNIR
import com.pajato.lapcounter.core.Ship.TIALFI
import com.pajato.lapcounter.core.Ship.TIR
import com.pajato.lapcounter.core.Ship.TOR
import com.pajato.lapcounter.core.Ship.ULUR
import com.pajato.lapcounter.core.Ship.VALI
import com.pajato.lapcounter.core.Ship.VAR
import com.pajato.lapcounter.core.Ship.VE
import com.pajato.lapcounter.core.Ship.VIDAR
import com.pajato.lapcounter.core.Ship.VILHJALM
import com.pajato.lapcounter.core.Ship.VILI


internal enum class LongshipCategory(override val item: SelectorItem, override val list: List<SelectorItem>) :
    SelectorType {
    A(
        CategoryItem("A"),
        listOf<SelectorItem>(AEGIR.item, ALRUNA.item, ALSVIN.item, ATLA.item)
    ),
    B(
        CategoryItem("B"),
        listOf<SelectorItem>(BALDUR.item, BRAGI.item, BURI.item)
    ),
    D(
        CategoryItem("D"),
        listOf<SelectorItem>(DELLING.item)
    ),
    E(
        CategoryItem("E"),
        listOf<SelectorItem>(EGIL.item, EINAR.item, EIR.item, EMBLA.item)
    ),
    F(
        CategoryItem("F"),
        listOf<SelectorItem>(FORSETI.item)
    ),
    G(
        CategoryItem("G"),
        listOf<SelectorItem>(GEFJON.item, GERSEMI.item, GULLVEIG.item)
    ),
    H(
        CategoryItem("H"),
        listOf<SelectorItem>(HEIMDAL.item, HERJA.item, HERMOD.item, HERVOR.item, HILD.item, HILN.item)
    ),
    I(
        CategoryItem("I"),
        listOf<SelectorItem>(IDI.item, IDUN.item, INGVI.item)
    ),
    J(
        CategoryItem("J"),
        listOf<SelectorItem>(JARL.item)
    ),
    K(
        CategoryItem("K"),
        listOf<SelectorItem>(KADIN.item, KARA.item, KVASIR.item)
    ),
    L(
        CategoryItem("L"),
        listOf<SelectorItem>(LIF.item, LOFN.item)
    ),
    M(
        CategoryItem("M"),
        listOf<SelectorItem>(MAGNI.item, MANI.item, MIMIR.item, MODI.item)
    ),
    N(
        CategoryItem("N"),
        listOf<SelectorItem>(NJORD.item)
    ),
    O(
        CategoryItem("O"),
        listOf<SelectorItem>(ODIN.item)
    ),
    R(
        CategoryItem("R"),
        listOf<SelectorItem>(RINDA.item, ROLF.item)
    ),
    S(
        CategoryItem("S"),
        listOf<SelectorItem>(SYGRUN.item, SYGYN.item, SKADI.item, SKIRNIR.item)
    ),
    T(
        CategoryItem("T"),
        listOf<SelectorItem>(TIALFI.item, TIR.item, TOR.item)
    ),
    U(
        CategoryItem("U"),
        listOf<SelectorItem>(ULUR.item)
    ),
    V(
        CategoryItem("V"),
        listOf<SelectorItem>(VALI.item, VAR.item, VE.item, VIDAR.item, VILHJALM.item, VILI.item)
    )
}
// internal enum class DouroCategory(override val item: SelectorItem, override val list: List<SelectorItem>) : SelectorType {}
// ELBE,
// SEINE,
// RUSSIA,
// ASIA,
// UKRAINE


internal enum class Ship(val item: ShipItem) {
    AEGIR(ShipItem("Aegir")),
    ALRUNA(ShipItem("Alruna")),
    ALSVIN(ShipItem("Alsvin")),
    ATLA(ShipItem("Atla")),

    BALDUR(ShipItem("Baldur")),
    BRAGI(ShipItem("Bragi")),
    BURI(ShipItem("Buri")),

    DELLING(ShipItem("Delling")),

    EGIL(ShipItem("Egil")),
    EINAR(ShipItem("Einar")),
    EIR(ShipItem("Eir")),
    EMBLA(ShipItem("Embla")),

    FORSETI(ShipItem("Forseti")),

    GEFJON(ShipItem("Gefjon")),
    GERSEMI(ShipItem("Gersemi")),
    GULLVEIG(ShipItem("Gullveig")),

    HEIMDAL(ShipItem("Heimdal")),
    HERJA(ShipItem("Herja")),
    HERMOD(ShipItem("Hermod")),
    HERVOR(ShipItem("Hervor")),
    HILD(ShipItem("Hild")),
    HILN(ShipItem("Hiln")),

    IDI(ShipItem("Idi")),
    IDUN(ShipItem("Idun")),
    INGVI(ShipItem("Ingvi")),

    JARL(ShipItem("Jarl")),

    KADIN(ShipItem("Kadin")),
    KARA(ShipItem("Kara")),
    KVASIR(ShipItem("Kvasir")),

    LIF(ShipItem("Lif")),
    LOFN(ShipItem("Lofn")),

    MAGNI(ShipItem("Magni")),
    MANI(ShipItem("Mani")),
    MIMIR(ShipItem("Mimir")),
    MODI(ShipItem("Modi")),

    NJORD(ShipItem("Njord")),

    ODIN(ShipItem("Odin")),

    RINDA(ShipItem("Rinda")),
    ROLF(ShipItem("Rolf")),

    SYGRUN(ShipItem("Sygrun")),
    SYGYN(ShipItem("Sygyn")),
    SKADI(ShipItem("Skadi")),
    SKIRNIR(ShipItem("Skirnir")),

    TIALFI(ShipItem("Tialfi")),
    TIR(ShipItem("Tir")),
    TOR(ShipItem("Tor")),

    ULUR(ShipItem("Ulur")),

    VALI(ShipItem("Vali")),
    VAR(ShipItem("Var")),
    VE(ShipItem("Ve")),
    VIDAR(ShipItem("Vidar")),
    VILHJALM(ShipItem("Vilhjalm")),
    VILI(ShipItem("Vili"))
}
