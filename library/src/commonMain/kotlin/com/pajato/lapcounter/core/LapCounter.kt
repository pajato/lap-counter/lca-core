package com.pajato.lapcounter.core

expect fun Double.convertToString(): String

data class Session(
    val cruiseName: String,
    val shipName: String,
    val start: String = getTimeStamp().toString(),
    var end: String = start,
    var lapCount: String = "0",
    var startLocation: String = "",
    var endLocation: String = ""
) {
    override fun toString(): String = "$start:$end:$cruiseName:$shipName:$lapCount:$startLocation:$endLocation"
}

class SessionInfo(lapCount: Int, lapsPerMile: Int) {
    private fun Double.convert(): String = convertToString()

    private val distance: Double = lapCount.toDouble() / lapsPerMile.toDouble()

    var lapCountAsString = lapCount.toString() // is var to support Android data-binding
    var distanceAsString = distance.convert() + " mi"
}

class LapCounter(persistenceDirectoryPath: String, fileName: String = "sessions") {
    internal lateinit var session: Session
    private val repo = mutableMapOf<String, Session>()
    private val persister = SessionPersister(persistenceDirectoryPath, fileName) { version, line ->
        val session = line.toSession(version)
        repo[session.start] = session
    }

    init {
        persister.prune { for (session in repo.values) persister.store(session.toString()) }
    }

    fun startSession(cruiseName: String, shipName: String) {
        session = Session(cruiseName, shipName)
        repo[session.start] = session
        persister.store(session.toString())
    }

    fun countLap(startLocation: String = "", endLocation: String = "") {
        fun updateSession() {
            session.apply {
                end = getTimeStamp().toString()
                lapCount = (lapCount.toInt() + 1).toString()
                this.startLocation = startLocation
                this.endLocation = endLocation
            }
        }

        updateSession()
        persister.store(session.toString())
    }

    fun getCurrentSessionInfo(): SessionInfo {
        val lapCount = session.lapCount.toInt()

        return SessionInfo(lapCount, 12)
    }

    fun getCumulativeSessionInfo(): SessionInfo {
        fun accumulateLaps(): Int {
            var sum = 0
            for (session in repo.values) {
                sum += session.lapCount.toInt()
            }
            return sum
        }

        val totalNumberOfLaps: Int = accumulateLaps()

        return SessionInfo(totalNumberOfLaps, 12)
    }

}

internal fun String.toSession(version: Int): Session {
    val fields = this.split(":")
    fun isValidVersion(): Boolean = when (version) {
        1 -> true
        else -> false
    }

    fun isValidReceiver(): Boolean = fields.size == 7

    check(isValidVersion()) { "Unsupported version: $version." }
    check(isValidReceiver()) { "Invalid session string: $this." }
    return Session(
        start = fields[0],
        end = fields[1],
        cruiseName = fields[2],
        shipName = fields[3],
        lapCount = fields[4],
        startLocation = fields[5],
        endLocation = fields[6]
    )
}