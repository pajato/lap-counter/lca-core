package com.pajato.lapcounter.core

import com.pajato.io.KFile
import com.pajato.io.createKotlinFile

internal class SessionPersister(directoryPath: String, fileName: String = "sessions", loader: (Int, String) -> Unit) :
    Persister(directoryPath, fileName, loader)

internal abstract class Persister(directoryPath: String, fileName: String, loadItems: (Int, String) -> Unit) {

    private val repoFile: KFile by lazy {
        require(!directoryPath.isBlank()) { ILLEGAL_DIRECTORY_NAME_MESSAGE }
        require(!fileName.isBlank()) { ILLEGAL_FILE_NAME_MESSAGE }
        createKotlinFile(directoryPath, fileName).apply {
            val lines = readLines()
            if (lines.isEmpty()) appendText("$PERSISTENCE_VERSION\n")
        }
    }

    init {
        val lines = repoFile.readLines()
        fun loadData() {
            val dataVersion = lines[0].toInt()
            if (lines.size > 1) for (index in 1 until lines.size) loadItems(dataVersion, lines[index])
        }

        check(lines.isNotEmpty()) { "Corrupt data file: empty!" }
        loadData()
    }

    fun prune(storeItems: () -> Unit) {
        repoFile.clear()
        repoFile.appendText("$PERSISTENCE_VERSION\n")
        storeItems()
    }

    fun store(record: String) {
        repoFile.appendText(record + "\n")
    }
}
