package com.pajato.lapcounter.core

import com.pajato.io.createKotlinFile
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class LapCounterTest {
    private val testDir = "build"
    private val cruiseName = "Rhine Getaway"
    private val shipName = "Viking Sigrun"
    private val testFile = createKotlinFile(testDir, "sessions")

    @BeforeTest
    fun setup() {
        testFile.apply {
            clear()
            appendText("$PERSISTENCE_VERSION\n")
        }
    }

    @Test
    fun `when lapCount is 0 verify session is consistent`() {
        val uut = LapCounter(testDir)
        uut.startSession(cruiseName, shipName)

        val startTS = uut.session.start
        val expected = 0
        assertEquals(startTS, uut.session.end)
        assertEquals(expected, uut.session.lapCount.toInt())

        assertEquals("0", uut.getCurrentSessionInfo().lapCountAsString)
        assertEquals("0.00 mi", uut.getCurrentSessionInfo().distanceAsString)
    }

    @Test
    fun `when lapCount is 0 verify countLap returns new lapCount equal to 1`() {
        val uut = LapCounter(testDir)
        val expected = 1
        uut.startSession(cruiseName, shipName)
        uut.countLap()
        assertEquals(expected, uut.session.lapCount.toInt())
    }

    @Test
    fun `when lapCount is 0 verify cumulative data is consistent`() {
        val uut = LapCounter(testDir)
        uut.startSession(cruiseName, shipName)
        val sessionInfo = uut.getCumulativeSessionInfo()
        val expected = "0"
        assertEquals(expected, sessionInfo.lapCountAsString)
        assertEquals("0.00 mi", sessionInfo.distanceAsString)
    }

    @Test
    fun `when lapCount is 1 verify cumulative data is consistent`() {
        val uut = LapCounter(testDir)
        uut.startSession(cruiseName, shipName)
        val expected = "1"
        uut.countLap()
        val session = uut.getCumulativeSessionInfo()
        assertEquals(expected, session.lapCountAsString)
    }

    @Test
    fun `when previous session data is available verify it is loaded`() {
        testFile.appendText("1572922173192:1572922175192:Rhine Getaway:Viking Sigrun:8::\n")
        testFile.appendText("1572922193192:1572922178192:Rhine Getaway:Viking Sigrun:12::\n")
        val uut = LapCounter(testDir)
        uut.startSession(cruiseName, shipName)
        val expected = "21"
        uut.countLap()
        val sessionInfo = uut.getCumulativeSessionInfo()
        assertEquals(expected, sessionInfo.lapCountAsString)
        assertEquals("1.75 mi", sessionInfo.distanceAsString)
    }

    @Test
    fun `when previous session data has one lap verify distance is correct`() {
        val uut = LapCounter(testDir)
        uut.startSession(cruiseName, shipName)
        val expected = "1"
        uut.countLap()
        val sessionInfo = uut.getCumulativeSessionInfo()
        assertEquals(expected, sessionInfo.lapCountAsString)
        assertEquals("0.08 mi", sessionInfo.distanceAsString)
    }

    @Test
    fun `make the optional filename explicit`() {
        val testFile = createKotlinFile(testDir, "not-sessions")
        testFile.clear()
        val uut = LapCounter(testDir, "not-sessions")
        uut.startSession(cruiseName, shipName)
        val content = testFile.readLines()
        assertEquals(2, content.size)
    }
}
