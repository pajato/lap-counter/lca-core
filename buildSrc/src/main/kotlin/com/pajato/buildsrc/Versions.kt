object Versions {
    const val KOTLIN = "1.3.71"
    const val CORE = "0.1.9"
    const val KFILE = "0.3.3"
    const val JACOCO = "0.8.5"
}
